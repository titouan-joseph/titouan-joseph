<!--
**titouan-joseph/titouan-joseph** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

### Hey 👋🏽, I'm [Titouan](https://github.com/Titouan-Joseph) 

I'm a engineering student at  [INSA Lyon 🦏](https://www.insa-lyon.fr/en/)

- 📫 How to reach me: [titouan-joseph@cicorella.net](mailto:titouan-joseph@cicorella.net)
- 🔭 I’m currently working on internship


  <img align="right" alt="GitHub Metrics" src="https://metrics.lecoq.io/titouan-joseph" />

**Languages and Tools:**

[<img src="https://img.icons8.com/color/48/000000/python.png"/>]()[<img src="https://img.icons8.com/color/48/000000/java-coffee-cup-logo.png"/>]() [<img src="https://img.icons8.com/color/48/000000/c-programming.png"/>]() [<img src="https://img.icons8.com/color/48/000000/javascript.png"/>]() [<img src="https://img.icons8.com/color/48/000000/selenium-test-automation.png"/>]() [<img src="https://img.icons8.com/color/48/000000/git.png"/>]() [<img src="https://img.icons8.com/color/48/000000/console.png"/>]() [<img src="https://img.icons8.com/color/48/000000/android-os.png"/>]() [<img src="https://img.icons8.com/color/48/000000/pycharm.png"/>]() [<img src="https://img.icons8.com/color/48/000000/virtualbox.png"/>]() [<img src="https://img.icons8.com/color/48/000000/windows-10.png"/>]()

[<img src="https://img.icons8.com/color/48/000000/linux.png"/>]() [<img src="https://img.icons8.com/color/48/000000/nginx.png"/>]() [<img src="https://img.icons8.com/color/48/000000/raspberry-pi.png"/>]() [<img src="https://img.icons8.com/color/48/000000/docker.png"/>]()[<img src="https://img.icons8.com/color/48/000000/visual-studio-code-2019.png"/>]()

**My social networks :**

[<img src='https://img.icons8.com/fluent/48/000000/github.png' alt="github">](https://github.com/titouan-joseph)  [<img src='https://img.icons8.com/color/48/000000/linkedin.png' alt='linkedin'>](https://www.linkedin.com/in/titouan-joseph-revol/)  [<img src='https://img.icons8.com/color/48/000000/instagram-new.png' alt='instagram'>](https://www.instagram.com/tit_ci/)  [<img src='https://img.icons8.com/color/48/000000/twitter.png' alt='twitter'>](https://twitter.com/tit_ci) [<img src="https://img.icons8.com/color/48/000000/facebook.png"/>](https://www.facebook.com/titre01) [<img src="https://img.icons8.com/fluent/48/000000/domain.png" alt="domain"/>](https://titouan-joseph.cicorella.net)

<details>
 <summary>👨‍💻 <b>Programming stats (Click to expand)</b>: </summary>
<!--START_SECTION:waka-->
![Code Time](http://img.shields.io/badge/Code%20Time-937%20hrs%2038%20mins-blue)

**🐱 My GitHub Data** 

> 🏆 214 Contributions in the Year 2022
 > 
> 📦 88.7 kB Used in GitHub's Storage 
 > 
> 🚫 Not Opted to Hire
 > 
> 📜 31 Public Repositories 
 > 
> 🔑 2 Private Repositories  
 > 
**I'm a Night 🦉** 

```text
🌞 Morning    93 commits     ████░░░░░░░░░░░░░░░░░░░░░   15.66% 
🌆 Daytime    181 commits    ███████░░░░░░░░░░░░░░░░░░   30.47% 
🌃 Evening    288 commits    ████████████░░░░░░░░░░░░░   48.48% 
🌙 Night      32 commits     █░░░░░░░░░░░░░░░░░░░░░░░░   5.39%

```
📅 **I'm Most Productive on Tuesday** 

```text
Monday       100 commits    ████░░░░░░░░░░░░░░░░░░░░░   16.84% 
Tuesday      143 commits    ██████░░░░░░░░░░░░░░░░░░░   24.07% 
Wednesday    118 commits    █████░░░░░░░░░░░░░░░░░░░░   19.87% 
Thursday     67 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   11.28% 
Friday       48 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   8.08% 
Saturday     55 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   9.26% 
Sunday       63 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   10.61%

```


📊 **This Week I Spent My Time On** 

```text
⌚︎ Time Zone: Europe/Paris

💬 Programming Languages: 
Other                    2 hrs 30 mins       █████████░░░░░░░░░░░░░░░░   37.95% 
Bash                     1 hr 31 mins        █████░░░░░░░░░░░░░░░░░░░░   22.96% 
YAML                     1 hr 16 mins        ████░░░░░░░░░░░░░░░░░░░░░   19.2% 
JSON                     31 mins             ██░░░░░░░░░░░░░░░░░░░░░░░   7.91% 
TypeScript               24 mins             █░░░░░░░░░░░░░░░░░░░░░░░░   6.05%

🔥 Editors: 
VS Code                  3 hrs 30 mins       █████████████░░░░░░░░░░░░   53.04% 
Bash                     3 hrs 6 mins        ███████████░░░░░░░░░░░░░░   46.96%

🐱‍💻 Projects: 
overbookd-mono           6 hrs 11 mins       ███████████████████████░░   93.74% 
Terminal                 24 mins             █░░░░░░░░░░░░░░░░░░░░░░░░   6.26%

💻 Operating System: 
Linux                    6 hrs 36 mins       █████████████████████████   100.0%

```

**I Mostly Code in Python** 

```text
Python                   19 repos            ██████████████░░░░░░░░░░░   55.88% 
JavaScript               4 repos             ███░░░░░░░░░░░░░░░░░░░░░░   11.76% 
HTML                     2 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   5.88% 
C                        2 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   5.88% 
MATLAB                   2 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   5.88%

```



 Last Updated on 22/09/2022 14:17:41 UTC
<!--END_SECTION:waka-->

</details>

<b>Projects programming stats</b>:
|Project|Time|
|:-----:|:--:|
|Overbookd| [![wakatime](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/d8c55d07-5b66-4500-8928-c8628ca2fc78.svg)](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/d8c55d07-5b66-4500-8928-c8628ca2fc78) |
|website24maker|[![wakatime](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/0d2d9294-0be7-4646-9c4f-7169f120f4e7.svg)](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/0d2d9294-0be7-4646-9c4f-7169f120f4e7)|
|AstusBot|[![wakatime](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/e6f09298-a37c-4761-b8d4-5ec7312fd79f.svg)](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283/project/e6f09298-a37c-4761-b8d4-5ec7312fd79f)|
|Total programming|[![wakatime](https://wakatime.com/badge/user/07f10887-f0d8-43c1-b329-d19c27059283.svg)](https://wakatime.com/@07f10887-f0d8-43c1-b329-d19c27059283)|

Icons by [icones8](https://icones8.fr/)
